import React, { Component } from 'react';
import {Map, TileLayer, Polyline} from 'react-leaflet';
import {AircraftContainer} from "./Aircraft";
import {ABContainer, FARPContainer} from './Bases';
import {ShipContainer} from "./Ships";
import {GroundUnits} from './GroundUnits'

export class GCIMap extends Component {

  render() {
    const position = [this.props.lat, this.props.lng];

    return(
      <Map animate={true} center={position} zoom={this.props.zoom}>
        <TileLayer
          attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
          url="https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png"
        />
        <Polyline smoothFactor="1" weight="1.5" opacity=".4" color="lime" positions={this.props.bluelines}/>
        <Polyline smoothFactor="1" weight="1.5" opacity=".4" color="red" positions={this.props.redlines}/>
        <AircraftContainer filters={this.props.filters} aircraft={this.props.aircraft} />
        <ABContainer filters={this.props.filters} airbases={this.props.airbases} />
        <FARPContainer filters={this.props.filters} farps={this.props.farps} />
        <ShipContainer filters={this.props.filters} ships={this.props.ships}/>
        <GroundUnits filters={this.props.filters} ground_units={this.props.ground_units} />
      </Map>
    );
  }
}