import React from 'react'

export const FilterList = (props) => {
    const filters = [];
    for (let f in props.filters) {
        const filter = props.filters[f];
        filters.push(<tr key={filter.name} ><td><input type="checkbox" onChange={() => { props.checkFilter(filter.name)}} defaultChecked={filter.checked} name={filter.name} /></td><td>{filter.name}</td></tr> );
    }

    return(
        <div id="filterDiv">
            <table>
                <tbody>
                    {filters}
                </tbody>
            </table>
        </div>
    )
};
