import React, {Component} from 'react'
import Leaflet from 'leaflet';
import {Marker, Popup} from "react-leaflet";

export class GroundUnits extends Component {
    constructor(props) {
        super(props);
        this.iconSize = [24, 24];

        this.redradarIcon = {
            types: ['S-300PS 40B6M tr', 'S-300PS 40B6MD sr', 'Kub 1S91 str', '1L13 EWR', 'Dog Ear radar'],
            icon: new Leaflet.Icon({
                iconUrl: require('./icons/REDRADAR.svg'),
                iconSize: this.iconSize
            })
        };

        this.redsamIcon = {
            types: ['S-300PS 5P85C ln', 'Kub 2P25 ln', 'Osa 9A33 ln'],
            icon: new Leaflet.Icon({
                iconUrl: require('./icons/REDAD.svg'),
                iconSize: this.iconSize
            })
        };

        this.redaaaIcon = {
            types: ['ZSU-23-4 Shilka', '2S6 Tunguska'],
            icon: new Leaflet.Icon({
                iconUrl: require('./icons/REDAAA.svg'),
                iconSize: this.iconSize
            })
        };

        this.blueradarIcon = {
            types: ['Hawk sr', 'Hawk pcp'],
            icon: new Leaflet.Icon({
                iconUrl: require('./icons/BLUERADAR.svg'),
                iconSize: this.iconSize
            })
        };

        this.bluesamIcon = {
            types: [],
            icon: new Leaflet.Icon({
                iconUrl: require('./icons/BLUEAD.svg'),
                iconSize: this.iconSize
            })
        };

        this.blueaaaIcon = {
            types: [],
            icon: new Leaflet.Icon({
                iconUrl: require('./icons/BLUEAAA.svg'),
                iconSize: this.iconSize
            })
        };

        this.blueunknownIcon = {
            types: [],
            icon: new Leaflet.Icon({
                iconUrl: require('./icons/BLUEUNKNOWN.svg'),
                iconSize: this.iconSize
            })
        };

        this.redunknownIcon = {
            types: [],
            icon: new Leaflet.Icon({
                iconUrl: require('./icons/REDUNKNOWN.svg'),
                iconSize: this.iconSize
            })
        };

        this.icons = [this.blueaaaIcon, this.blueradarIcon, this.bluesamIcon, this.redaaaIcon, this.redradarIcon, this.redsamIcon, this.blueunknownIcon, this.redunknownIcon];
    }

    render() {
        const units = [];

        if (this.props.filters['SAM/AAA'].checked) {
            this.props.ground_units.forEach((object) => {
                let this_icon = null;
                this.icons.forEach((icon) => {
                    if (icon.types.includes(object.Name)) {
                        this_icon = icon.icon;
                    }
                });

                if (this_icon !== null) {
                    units.push(<Marker key={object.id} icon={this_icon}
                                       position={[object.LatLongAlt.Lat, object.LatLongAlt.Long]}>
                        <Popup>
                            <table cellSpacing={0} className="table table-borderless table-sm acID">
                                <tbody>
                                <tr>
                                    <td><strong>{object.Name}</strong></td>
                                </tr>
                                <tr>
                                    <td><strong>TYPE:</strong></td>
                                    <td>{object.Name}</td>
                                </tr>
                                <tr>
                                    <td><strong>ALT:</strong></td>
                                    <td>{Math.floor(object.LatLongAlt.Alt)} FT</td>
                                </tr>
                                <tr>
                                    <td><strong>HDG:</strong></td>
                                    <td>{Math.floor(object.Heading)}</td>
                                </tr>
                                </tbody>
                            </table>
                        </Popup>
                    </Marker>)
                }
            });
        }

        return units;
    };
}