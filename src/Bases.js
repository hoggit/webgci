import React, {Component} from 'react';
import {Marker} from 'react-leaflet';
import Leaflet from 'leaflet';
import './App.css';

export class ABTable extends Component {
    render() {
        return (
            <table className="actable sidebar-table table table-sm" id="abtable">
                <tbody>
                {this.props.airbases.map((ab) => {
                    if (ab.Name !== 'woRunWay' && this.props.objectives.includes(ab.Name)) {
                        let classname = 'neutralap';
                        let owner = "NEUTRAL";

                        if (ab.CoalitionID === 2) {
                            classname = 'blueap';
                            owner = 'BLUE';
                        } else if (ab.CoalitionID === 1) {
                            classname = 'redap';
                            owner = 'RED';
                        }

                        return (<tr key={ab.id} className={classname}
                                    onClick={() => this.props.mapRecenter([ab.LatLongAlt.Lat, ab.LatLongAlt.Long])}>
                            <td><strong>{ab.Name}</strong></td>
                            <td>Controlled by {owner}</td>
                        </tr>)
                    } else {
                        return null;
                    }
                })}
                </tbody>
            </table>
        )
    }
}

export class ABContainer extends React.Component {
    constructor(props) {
        super(props);
        this.iconSize = [32, 32];
        this.icons = {
            "red": new Leaflet.Icon({
                iconUrl: require('./icons/REDAP.png'),
                iconSize: this.iconSize,
                className: 'apicon'
            }),
            "blue": new Leaflet.Icon({
                iconUrl: require('./icons/BLUEAP.png'),
                iconSize: this.iconSize,
                className: 'apicon'
            }),
            "neutral": new Leaflet.Icon({
                iconUrl: require('./icons/NEUTRALAP.png'),
                iconSize: this.iconSize,
                className: 'apicon'
            }),

        };
    }

    render() {
        const ab_markers = [];
        this.props.airbases.forEach((ab) => {
            if (ab.Name !== 'woRunWay') {
                const ab_location = [ab.LatLongAlt.Lat, ab.LatLongAlt.Long];
                let icon = this.icons['neutral'];

                if (ab.CoalitionID === 1) {
                    icon = this.icons['red'];
                } else if (ab.CoalitionID === 2) {
                    icon = this.icons['blue'];
                }

                ab_markers.push(<Marker key={ab.id} position={ab_location} icon={icon}/>)
            }
        });

        return ab_markers;
    }
}

export class FARPTable extends React.Component {
    render() {
        return (
            <table className="farptable sidebar-table table table-sm" id="farptable">
                <tbody>
                {this.props.farps.map((farp) => {
                    let classname = 'neutralfarp';
                    let owner = "NEUTRAL";
                    if (farp.CoalitionID === 2) {
                        classname = 'bluefarp';
                        owner = 'BLUE';
                    } else if (farp.CoalitionID === 1) {
                        classname = 'redfarp';
                        owner = 'RED';
                    }

                    return (<tr key={farp.id} className={classname}
                                onClick={() => this.props.mapRecenter([farp.LatLongAlt.Lat, farp.LatLongAlt.Long])}>
                        <td><strong>{farp.Id}</strong></td>
                        <td>Controlled by {owner}</td>
                    </tr>)
                })}
                </tbody>
            </table>
        )
    }
}

export class FARPContainer extends React.Component {
    constructor(props) {
        super(props);
        this.iconSize = [48, 48];
        this.icons = {
            "red": new Leaflet.Icon({
                iconUrl: require('./icons/REDWARE.svg'),
                iconSize: this.iconSize,
                className: 'farpicon'
            }),
            "blue": new Leaflet.Icon({
                iconUrl: require('./icons/BLUEWARE.svg'),
                iconSize: this.iconSize,
                className: 'farpicon'
            }),
            "neutral": new Leaflet.Icon({
                iconUrl: require('./icons/NEUTRALWARE.svg'),
                iconSize: this.iconSize,
                className: 'farpicon'
            }),

        };
    }

    render() {
        const farp_markers = [];
        this.props.farps.forEach((farp) => {
            const farp_location = [farp.LatLongAlt.Lat, farp.LatLongAlt.Long];
            const farp_side = farp.CoalitionID;
            let icon = this.icons['neutral'];

            if (farp_side === 1) {
                icon = this.icons['red'];
            } else if (farp_side === 2) {
                icon = this.icons['blue'];
            }

            farp_markers.push(<Marker key={farp.id} position={farp_location} icon={icon}/>)
        });

        return farp_markers;
    }
};
