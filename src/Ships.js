import React, {Component} from 'react'
import {Marker, Popup} from 'react-leaflet'
import Leaflet from 'leaflet';

export class ShipContainer extends Component {
    constructor(props) {
        super(props);
        this.blueIcons =[{
            types:["Stennis"],
            icon: new Leaflet.Icon({
                iconUrl: require('./icons/BLUECARRIER.svg'),
                iconSize: [32,32]
            })
        },{
            types:["TICONDEROG"],
            icon: new Leaflet.Icon({
                iconUrl: require('./icons/BLUECRUISER.svg'),
                iconSize: [32,32]
            })
        },{
            types:["LHA_Tarawa"],
            icon: new Leaflet.Icon({
                iconUrl: require('./icons/BLUELHA.svg'),
                iconSize: [32,32]
            })
        }];

        this.redIcons =[{
            types:["Stennis"],
            icon: new Leaflet.Icon({
                    iconUrl: require('./icons/BLUECARRIER.svg'),
                    iconSize: [32,32]
                })
        }];

        this.blueUnknown = new Leaflet.Icon({ iconUrl: require('./icons/BLUEUNKNOWN.svg'), iconSize: [20,24]});
        this.blueUnknown = new Leaflet.Icon({ iconUrl: require('./icons/REDUNKNOWN.svg'), iconSize: [20,24]});
    }

    render() {
        const ships = [];
        this.props.ships.forEach((ship) => {
            const iconSet = (ship.CoalitionID === 1) ? this.redIcons : this.blueIcons;
            let icon = (ship.CoalitionID === 1) ? this.redUnknown : this.blueUnknown;
            iconSet.forEach((i) => {
                if (i.types.includes(ship.Name)) {
                    icon = i.icon;
                }
            });
            ships.push(<Ship icon={icon} lat={ship.LatLongAlt.Lat} long={ship.LatLongAlt.Long} name={ship.Name} heading={Math.floor(ship.Heading)+"°"} />)
        });

        return ships;
    }
}

const Ship = (props) => {
   return (<Marker icon={props.icon} position={[props.lat, props.long]}>
    <Popup>
          <table cellSpacing={0} className="table table-borderless table-sm acID">
            <tbody>
            <tr><td><strong>{props.name}</strong></td></tr>
            <tr><td><strong>HDG:</strong></td> <td>{props.heading}</td></tr>
            </tbody>
          </table>
        </Popup>
   </Marker>)
} ;