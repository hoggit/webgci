import React, {Component} from 'react';
import {ABTable, FARPTable} from './Bases';

export class ObjectivesTable extends Component {
    render() {
        return (
            <div id="objectives">
                <h3>Theater Objectives</h3>
                <ul className="nav nav-tabs" id="objectivesTabs" role="tablist">
                    <li className="nav-item">
                        <a className="nav-link active" id="airfieldsTab" data-toggle="tab" href="#airfieldsTable"
                           title="Airbases">
                            <img className="tab-header-icon" src={require('./icons/NEUTRALAP.png')} alt="Airbases"/>
                        </a>
                    </li>

                    <li className="nav-item">
                        <a className="nav-link" id="FARPTab" data-toggle="tab" href="#FARPTable"
                           title="Warehouses/FARPs">
                            <img className="tab-header-icon" src={require('./icons/BLUEWARE.svg')} alt="Warehouses/FARPS"/>
                            <img className="tab-header-icon" src={require('./icons/REDWARE.svg')}
                                 alt="Warehouses/FARPS"/>
                        </a>
                    </li>


                    <li className="nav-item hidden">
                        <a className="nav-link" id="SAMTab" data-toggle="tab" href="#SAMTable" title="SAM Sites">
                            <img className="tab-header-icon" src={require('./icons/REDSAM.png')} alt="SAM Sites"/>
                        </a>
                    </li>

                    <li className="nav-item hidden">
                        <a className="nav-link" id="BAITab" data-toggle="tab" href="#BAITable">BAI</a>
                    </li>
                </ul>

                <div className="tab-content" id="ObjectivesContent">
                    <div className="tab-pane show active" id="airfieldsTable">
                        <ABTable objectives={this.props.objectives} airbases={this.props.airbases}
                                 mapRecenter={this.props.mapRecenter}/>
                    </div>

                    <div className="tab-pane" id="FARPTable">
                        <FARPTable farps={this.props.farps} mapRecenter={this.props.mapRecenter}/>
                    </div>

                    <div className="tab-pane" id="SAMTable">

                    </div>

                    <div className="tab-pane show active" id="BAITable">

                    </div>
                </div>
            </div>
        )
    }
}

