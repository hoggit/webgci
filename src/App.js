import React, {Component} from 'react';
import {MapContainer} from "./MapContainer";
import './App.css';
import {AircraftTable} from "./Aircraft";
import {ObjectivesTable} from "./Objectives";
import {FilterList} from "./Filters";

// eslint-disable-next-line
Number.prototype.toHHMMSS = function () {
    var sec_num = parseInt(this, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return hours+':'+minutes+':'+seconds;
}

const phase_one_objectives = [
                  'Krasnodar-Center',
                  'Anapa-Vityazevo',
                  'Novorossiysk',
                  'Maykop-Khanskaya',
                  'Krymsk',
                  'Gelendzhik',
                  'Krasnodar-Pashkovsky'
                ]
const phase_two_objectives = [
                  'Maykop-Khanskaya',
                  'Beslan',
                  'Sukhumi-Babushara',
                  'Gudauta',
                  'Sochi-Adler',
                  'Mineralnye Vody',
                  'Nalchik',
                  'Mozdok'
                ]
class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            theater_objectives: (phase) => {
              if (phase === undefined) {
                console.log("Phase undefined");
                return [];
              }
              if (phase === 1) {
                console.log("In phase 1");
                //return phase_one_objectives;
                return phase_one_objectives;
              }
              if (phase === 2) {
                console.log("In phase 2");
                return phase_two_objectives;
              }
              return [];
            },
            history_length: 20,
            updateTime: 0,
            uptime: 0,
            temperature: 0,
            wx_icon: '/cloudy.png',
            players: 0,
            maxPlayers: 0,
            theater: "",
            serverName: "",
            zoom: 9,
            lat: 44.5,
            lng: 38.7,
            blueAC: [],
            redAC: [],
            FARPS: [],
            ships: [],
            airports: [],
            ground_units:[],
            bluehistory: [],
            redhistory: [],
            bluelines: [],
            redlines: [],
            phase: 0,
            filters: {
                'Aircraft': {
                    name: "Aircraft",
                    value: 1,
                    checked: true
                },
                /*'Helicopters': {
                    name: "Helicopters",
                    value: 1,
                    checked: true
                },*/
                'Ejected Pilots': {
                    name: "Ejected Pilots",
                    value: 0,
                    checked: false
                },
                'SAM/AAA': {
                    name: "SAM/AAA",
                    value: 1,
                    checked: true
                },
                /*'Ground Units': {
                    name: "Ground Units",
                    value: 1,
                    checked: true
                }*/
            }
        };

        setInterval(() => {
            this.fetchState();
        }, 10000);

        this.translateState = this.translateState.bind(this);
        this.fetchState = this.fetchState.bind(this);
        this.checkFilter = this.checkFilter.bind(this);

        this.fetchState();
    };

    checkFilter(clicked_filter) {
        const filters = this.state.filters;
        filters[clicked_filter].checked = !filters[clicked_filter].checked;
        this.setState({filters: filters});
    }

    fetchState() {
        const xhr = new XMLHttpRequest();
        xhr.open("GET", "https://state.hoggitworld.com");
        xhr.onload = () => {
            if (xhr.status === 200) {
                const serverState = JSON.parse(xhr.responseText);
                this.translateState(serverState);
            }
        };

        xhr.send()
    }

    rad2deg(rad) {
        return rad * 180 / Math.PI;
    }

    translateState = (serverState) => {
        let state = {};
        const bluelines = [];
        const redlines = [];
        const bhistory = this.state.bluehistory;
        const rhistory = this.state.redhistory;
        const blueAC = [];
        const redAC = [];
        const FARPS = [];
        const ships = [];
        const ground_units = [];

        state.airports = serverState.airports || [];
        state.players = serverState.players;
        state.maxPlayers = serverState.maxPlayers;
        state.missionName = serverState.missionName;
        state.phase = serverState.missionName.includes("P2") ? 2 : 1;
        console.log("State set to " + state.phase);
        state.theater = serverState.theater;
        state.serverName = serverState.serverName;
        state.updateTime = serverState.updateTime;
        state.uptime = serverState.uptime;
        state.temperature = serverState.wx.season.temperature;
        state.receivedTime = Date.now();

        if (serverState.wx.clouds.iprecptns == 0) {
            if (serverState.wx.clouds.density <= 2) {
                state.wx_icon = '/clear.png';
            } else if (serverState.wx.clouds.density > 2 && serverState.wx.clouds.density <= 8) {
                state.wx_icon = '/partly_cloudy.png';
            } else if (serverState.wx.clouds.density > 8) {
                state.wx_icon = '/cloudy.png';
            }
        } else if (serverState.wx.clouds.iprecptns == 1) {
            state.wx_icon = '/rain.png';
        }  else if (serverState.wx.clouds.iprecptns == 2) {
            state.wx_icon = '/storm.png';
        }

        const objects = serverState.objects.map(x => {
            x.Heading = this.rad2deg(x.Heading);
            return x;
        });

        objects.forEach((object) => {
            switch (object.Type.level1) {
                case 1:
                    // handle aircraft
                    // No this isn't a typo, those pesky Ruskies
                    if (object.CoalitionID === 2) {
                        object.side = 2;
                        blueAC.push(object);
                    } else if (object.CoalitionID === 1) {
                        object.side = 1;
                        redAC.push(object);
                    }
                    break;
                case 2:
                    ground_units.push(object);
                    break;
                case 3:
                    ships.push(object);
                    break;
                case 5:
                    // Handle FARPS
                    if (object.Type.level1 === 5 && object.Type.level2 === 13) {
                        FARPS.push(object);
                    }
                    break;
                default:
                    //console.log("Not handling this type yet");
                    break;
            }
        });

        blueAC.concat(redAC).forEach((ac) => {
            let history;
            if (ac.side === 2) {
                history = bhistory;
            } else {
                history = rhistory;
            }

            if (history[ac.id] === undefined) {
                history[ac.id] = [];
            }

            history[ac.id].push([ac.LatLongAlt.Lat, ac.LatLongAlt.Long]);
            if (history[ac.id].length >= this.state.history_length) {
                console.log("SHIFTING");
                history[ac.id].shift();
            }
        });

        for (let x in bhistory) {
            bluelines.push(bhistory[x])
        }

        for (let x in rhistory) {
            redlines.push(rhistory[x])
        }

        state.blueAC = blueAC;
        state.redAC = redAC;
        state.FARPS = FARPS;
        state.ships = ships;
        state.ground_units = ground_units;
        state.redlines = redlines;
        state.bluelines = bluelines;
        state.bluehistory = bhistory;
        state.redhistory = rhistory;


        this.setState(state);
    };

    mapRecenter = (latlng) => {
        this.setState({
            zoom: 11,
            lat: latlng[0],
            lng: latlng[1]
        })
    };

    componentDidMount = () => {
        this.interval = setInterval(() => this.setState({}), 1000)
    }

    uptime = () => {
        const now = Date.now()
        const diff_seconds = (now - this.state.receivedTime) / 1000
        return (this.state.uptime + diff_seconds).toHHMMSS()
    }

    render() {
        return (
            <div className="d-flex" style={{height:'100%', width:'100%'}}>
                <div id="leftColumn" className="d-flex flex-column">
                    <AircraftTable filters={this.state.filters} mapRecenter={this.mapRecenter}
                                   aircraft={this.state.blueAC.concat(this.state.redAC)} />

                    <ObjectivesTable filters={this.state.filters} objectives={this.state.theater_objectives(this.state.phase)}
                                     mapRecenter={this.mapRecenter} farps={this.state.FARPS}
                                     airbases={this.state.airports}/>
                </div>
                <div className='d-flex' style={{zIndex:1, position:'relative', flex: 1}}>
                    <FilterList checkFilter={this.checkFilter} filters={this.state.filters}/>
                    <div id='uptimeDiv'>
                        <div id='weatherDiv'>
                            <img height='64' src={this.state.wx_icon} />
                            <strong style={{'margin-left':10, 'font-size': 24}}>{this.state.temperature} °C</strong>
                        </div>

                        {this.state.serverName} <br />
                        {this.state.missionName} <br />
                        Mission Time Elapsed: {this.uptime()}
                    </div>
                    <MapContainer
                        loaded={true}
                        doubleClickZoom={false}
                        zoom={this.state.zoom}
                        lat={this.state.lat}
                        lng={this.state.lng}
                        aircraft={this.state.blueAC.concat(this.state.redAC)}
                        redhistory={this.state.redhistory}
                        bluehistory={this.state.bluehistory}
                        redlines={this.state.redlines}
                        bluelines={this.state.bluelines}
                        airbases={this.state.airports}
                        farps={this.state.FARPS}
                        objectives={this.state.theater_objectives}
                        ships={this.state.ships}
                        filters={this.state.filters}
                        ground_units={this.state.ground_units}
                    />
                </div>
            </div>
                /*<section id="app">
                    <FilterList checkFilter={this.checkFilter} filters={this.state.filters}/>
                    <div id="leftColumn">
                        <AircraftTable filters={this.state.filters} mapRecenter={this.mapRecenter}
                                       aircraft={this.state.blueAC.concat(this.state.redAC)}/>
                        <ObjectivesTable filters={this.state.filters} objectives={this.state.theater_objectives}
                                         mapRecenter={this.mapRecenter} farps={this.state.FARPS}
                                         airbases={this.state.airports}/>
                    </div>
                    <div id="map">
                        <MapContainer
                            loaded={true}
                            doubleClickZoom={false}
                            zoom={this.state.zoom}
                            lat={this.state.lat}
                            lng={this.state.lng}
                            aircraft={this.state.blueAC.concat(this.state.redAC)}
                            redhistory={this.state.redhistory}
                            bluehistory={this.state.bluehistory}
                            redlines={this.state.redlines}
                            bluelines={this.state.bluelines}
                            airbases={this.state.airports}
                            farps={this.state.FARPS}
                            objectives={this.state.theater_objectives}
                            ships={this.state.ships}
                            filters={this.state.filters}
                            ground_units={this.state.ground_units}
                        />
                    </div>
                </section>
            </div>*/
        );
    }
}

export default App;
