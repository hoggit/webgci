import React, { Component } from 'react';
import {Marker, Popup} from 'react-leaflet';
import Leaflet from 'leaflet';

export class AircraftTable extends Component {
  render() {
    const racs = [];
    const bacs = [];
    let curList;

    if (this.props.filters['Aircraft'].checked) {

        this.props.aircraft.forEach((aircraft) => {
            if (aircraft.side === 1) {
                curList = racs;
            } else {
                curList = bacs;
            }

            curList.push(<AircraftTableItem key={aircraft.id}
                                            playerName={aircraft.UnitName}
                                            name={aircraft.Name}
                                            type={aircraft.Name}
                                            lat={aircraft.LatLongAlt.Lat}
                                            lng={aircraft.LatLongAlt.Long}
                                            heading={aircraft.Heading}
                                            height={aircraft.LatLongAlt.Alt}
                                            mapRecenter={this.props.mapRecenter}
            />)
        });
    }

    return (
      <div id="aircraft">
        <h3>Aircraft</h3>
        <ul className="nav nav-tabs" id="acsideTab" role="tablist">
          <li className="nav-item">
            <a className="nav-link active" id="blueACTab" data-toggle="tab" href="#blueACTable">Blue</a>
          </li>

          <li className="nav-item">
            <a className="nav-link" id="redACTab" data-toggle="tab" href="#redACTable">Red</a>
          </li>
        </ul>

        <div className="tab-content" id="ACContent">
          <div className="tab-pane show active" id="blueACTable">
            <div className="actableContainer">
            <table className="blueacs actable table table-bordered table-sm sidebar-table">
              <tbody>
                {bacs}
              </tbody>
            </table>
            </div>
          </div>

          <div className="tab-pane" id="redACTable">
            <table className="redacs actable table table-bordered table-sm sidebar-table">
              <tbody>
                {racs}
              </tbody>
            </table>
          </div>
        </div>
      </div>

    )
  }
}

const AircraftTableItem = (props) => {
  return (
    <tr className="aircraft-item" onClick={() => props.mapRecenter([props.lat, props.lng])} >
      <td>
        <div>
          <strong>
            {props.name}
          </strong>
        </div> {props.playerName &&
        <div>{props.playerName}</div>}
      </td>

      <td>
        {props.name}
      </td>

      <td>
        {Math.floor(props.height * 3.28)} FT
      </td>
    </tr>
  )
};

export class AircraftContainer extends Component {
  constructor(props) {
    super(props);
    this.iconSize = [27, 34];
    this.blueicons =[
      {types:["L-39ZA", "FA-18C_hornet", "F-15C", "F-5E-3", "M-2000C", "MiG-21Bis"], icon: new Leaflet.Icon({
        iconUrl: require('./icons/BLUEFIGHTER.svg'),
        iconSize: this.iconSize
      })},
      {types:["A-10C", "AJS37", "AV8BNA", "Su-25T", "A-10A", "Su-25A"], icon:  new Leaflet.Icon({
        iconUrl: require('./icons/BLUEATTACK.svg'),
        iconSize: this.iconSize
      })},
      {types:["E-3A"], icon: new Leaflet.Icon({
        iconUrl: require('./icons/BLUEAWACS.svg'),
        iconSize: this.iconSize
      })},
      {types:["UH-1H", "Mi-8"], icon: new Leaflet.Icon({
        iconUrl: require('./icons/BLUEUTILHELO.svg'),
        iconSize: this.iconSize
      })},
      {types:["Ka-50", "SA342M", "SA342L"], icon: new Leaflet.Icon({
        iconUrl: require('./icons/BLUEATTACKHELO.svg'),
        iconSize: this.iconSize
      })},
      {types:["KC-135", "KC130"], icon: new Leaflet.Icon({
        iconUrl: require('./icons/BLUETANKER.svg'),
        iconSize: this.iconSize
      })},
      {types:["C-130"], icon: new Leaflet.Icon({
        iconUrl: require('./icons/BLUECARGO.svg'),
        iconSize: this.iconSize
      })}
    ];

    this.redicons = [
    {types:["MiG-29S", "MiG-29A", "Su-27", "F-5E-3", "MiG-31", "J-11A", "MiG-21Bis"], icon: new Leaflet.Icon({
      iconUrl: require('./icons/REDFIGHTER.svg'),
      iconSize: this.iconSize
    })},
    {types:["Su-25T"], icon: new Leaflet.Icon({
      iconUrl: require('./icons/REDATTACK.svg'),
      iconSize: this.iconSize
    })},
    {types:["A-50"], icon: new Leaflet.Icon({
      iconUrl: require('./icons/REDAWACS.svg'),
      iconSize: this.iconSize
    })},
    {types:["Mi-26"], icon: new Leaflet.Icon({
      iconUrl: require('./icons/REDUTILHELO.svg'),
      iconSize: this.iconSize
    })},
    {types:[], icon: new Leaflet.Icon({
      iconUrl: require('./icons/REDATTACKHELO.svg'),
      iconSize: this.iconSize
    })},
    {types:["IL-78M"], icon: new Leaflet.Icon({
      iconUrl: require('./icons/REDTANKER.svg'),
      iconSize: this.iconSize
    })},
    {types:["IL-76MD"], icon: new Leaflet.Icon({
      iconUrl: require('./icons/REDCARGO.svg'),
      iconSize: this.iconSize
    })}];

    this.unknownblueicon = new Leaflet.Icon({
      iconUrl: require('./icons/BLUEUNKNOWN.svg'),
      iconSize: this.iconSize
    });

    this.unknownredicon = new Leaflet.Icon({
      iconUrl: require('./icons/REDUNKNOWN.svg'),
      iconSize: this.iconSize
    });

    this.paraicon = new Leaflet.Icon({
        iconUrl: require('./icons/para.png'),
        iconSize: this.iconSize
    });
  }

  render() {
    const acs = [];
    this.props.aircraft.forEach((aircraft) => {
      let icon = null;
      let iconList = this.blueicons;
      if (aircraft.side === 1) {
        iconList = this.redicons;
      }

      iconList.forEach((icon_obj) => {
        if (icon === null && icon_obj.types.includes(aircraft.Name)) {
          icon = icon_obj.icon;
        }
      });

      if (icon === null) {
        if (aircraft.side === 1) {
          icon = this.unknownredicon;
        } else {
          icon = this.unknownblueicon;
        }
      }

      if (this.props.filters['Aircraft'].checked && !aircraft.Name.toLowerCase().includes("pilot")) {
          acs.push(<Aircraft heading={Math.floor(aircraft.Heading)+"°"}
                             height={aircraft.LatLongAlt.Alt}
                             dcs_id={aircraft.id}
                             key={aircraft.id}
                             lat={aircraft.LatLongAlt.Lat}
                             lng={aircraft.LatLongAlt.Long}
                             name={aircraft.UnitName}
                             type={aircraft.Name}
                             icon={icon}
                             side={aircraft.side}
          />)
      } else if(aircraft.Name.toLowerCase().includes("pilot") && this.props.filters['Ejected Pilots'].checked) {
          acs.push(<Aircraft heading={Math.floor(aircraft.Heading)+"°"}
                             height={aircraft.LatLongAlt.Alt}
                             dcs_id={aircraft.id}
                             key={aircraft.id}
                             lat={aircraft.LatLongAlt.Lat}
                             lng={aircraft.LatLongAlt.Long}
                             name={aircraft.UnitName}
                             type={aircraft.Name}
                             icon={this.paraicon}
                             side={aircraft.side}
          />)
      }
    });
    return acs;
  }
}

const Aircraft = (props) => {
    return(<Marker icon={props.icon} position={[props.lat, props.lng]}>
        <Popup>
          <table cellSpacing={0} className="table table-borderless table-sm acID">
            <tbody>
            <tr><td><strong>{props.name}</strong></td></tr>
            <tr><td><strong>TYPE:</strong></td> <td>{props.type}</td></tr>
            <tr><td><strong>ALT:</strong></td> <td>{Math.floor(props.height * 3.28)} FT</td></tr>
            <tr><td><strong>HDG:</strong></td> <td>{props.heading}</td></tr>
            </tbody>
          </table>
        </Popup>
    </Marker>);
};
