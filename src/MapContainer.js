import React, {Component} from 'react';
import {GCIMap} from "./GCIMap";

export class MapContainer extends Component {
    render() {
        if (!this.props.loaded) {
            return <div>Loading...</div>
        } else {
            return (
                <GCIMap aircraft={this.props.aircraft}
                        zoom={this.props.zoom}
                        lat={this.props.lat}
                        lng={this.props.lng}
                        loaded={this.props.loaded}
                        redlines={this.props.redlines}
                        bluelines={this.props.bluelines}
                        airbases={this.props.airbases}
                        farps={this.props.farps}
                        ships={this.props.ships}
                        filters={this.props.filters}
                        ground_units={this.props.ground_units}
                />
            )
        }
    }
}
